<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GMaps;
use App\Grill;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $map = $this->getLocation();
        $grills = $this->getGrills();
        
        return view('home', compact('map', 'grills'));
    }

    public function getLocation()
    {
        $config['center'] = '9.006976, -79.511899';
        $config['zoom'] = '15';
        $config['map_height'] = '400px';
        $config['map_width'] = '100%';
        $config['geocodeCashing'] = true;
        $config['scrollwheel'] = false;

        GMaps::initialize($config);

        $this->getActualMarker($config);
        $this->getCircle($config);
        $this->getGrills();

        $map = GMaps::create_map();
        
        return $map;
    }

    public function getGrills()
    {
        $grills = Grill::All();

        foreach ($grills as $grill){
            
            $config['center'] = $grill->latitud . ', ' . $grill->longitud;
            $config['data'] = $grill->modelo . ', ' . $grill->descripcion;

            if (! $grill->disponible){

                $this->getUnavailableMarker($config);
            }

            $this->getAvailableMarker($config);

        }

        return $grills;
    }

    public function getCircle($config)
    {
        $circle['center'] = $config['center'];
        $circle['radius'] = '1000';

        return Gmaps::add_circle($circle);
    }

    public function getActualMarker($config)
    {
        $marker['position'] = $config['center'];
        $marker['infowindow_content'] = $marker['position'];

        return GMaps::add_marker($marker);
    }

    public function getAvailableMarker($config)
    {
        $marker['position'] = $config['center'];
        $marker['infowindow_content'] = $config['data'];
        $marker['icon'] = 'http://maps.google.com/mapfiles/ms/micons/green-dot.png';

        return GMaps::add_marker($marker);
    }

    public function getUnavailableMarker($config)
    {
        $marker['position'] = $config['center'];
        $marker['infowindow_content'] = $config['data'];
        $marker['icon'] = 'http://maps.google.com/mapfiles/ms/micons/lightblue.png';
        return GMaps::add_marker($marker);
    }
}
