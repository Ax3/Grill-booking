<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grill extends Model
{
    //

    protected $fillable = [
        'modelo', 
        'img', 
        'descripcion', 
        'disponible',
        'latitud', 
        'longitud',
    ];
}
