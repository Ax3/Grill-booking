<?php

use Illuminate\Database\Seeder;
use App\Grill;

class GrillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        Grill::create([
            'modelo' => 'BG-101', 
            'img' => 'grill-101', 
            'descripcion' => $faker->text,
            'disponible' => true,
            'latitud' => '9.013498', 
            'longitud' => '-79.509368', 
        ]);

        Grill::create([
            'modelo' => 'BG-102', 
            'img' => 'grill-102', 
            'descripcion' => $faker->text,
            'disponible' => true, 
            'latitud' => '9.007546', 
            'longitud' => '-79.512469',
        ]);

        Grill::create([
            'modelo' => 'BG-103', 
            'img' => 'grill-103', 
            'descripcion' => $faker->text, 
            'disponible' => true,
            'latitud' => '9.006354', 
            'longitud' => '-79.516523',
        ]);
    }
}
