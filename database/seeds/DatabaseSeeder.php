<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(GrillsTableSeeder::class);
        // $users = factory(App\User::class, 2)->create();
        // $grills = factory(App\Grill::class, 2)->create();
    }
}
