<?php

use Faker\Generator as Faker;

$factory->define(App\Grill::class, function (Faker $faker) {
    return [
        'modelo'=> str_random(5), 
        'img'=> 'path', 
        'descripcion'=> $faker->text, 
        'latitud'=> $faker->latitude(-90, 90), 
        'longitud'=> $faker->longitude(-180, 180)
    ];
});
