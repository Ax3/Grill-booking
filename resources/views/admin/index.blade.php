@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Listado de Parrillas en el sistema</div>

                <div class="card-body">
                    <table>
                        <thead>
                            <th class="text-center">Modelo</th>
                            <th class="text-center">Imagen</th>
                            <th class="text-center">Descripción</th>
                            <th class="text-center">Disponibilidad</th>
                            
                        </thead>
                        <tbody>
                            @foreach ($grills as $grill)
                                <tr>
                                    <th scope="row" class="text-center" style="width: 20%">{{ $grill->modelo }}</th>
                                    <td class="text-center" style="width: 20%"><img src="{{ asset('/'.$grill->img.'.jpg') }}" alt="" style="max-width:100px; max-height:100px"></td>
                                    <td class="text-center" style="width: 20%">{{ $grill->descripcion }}</td>
                                    <td class="text-center" style="width: 20%">{{ $grill->disponible }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
