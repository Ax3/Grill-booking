@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Mapa de Parrillas</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {!! $map['html'] !!}
                </div>
            </div>
        </div>

        <div class="col-md-3">
                <div class="card">
                    <div class="card-header">Parrillas En el Sistema</div>
    
                    <div class="card-body">
                        @foreach ($grills as $grill)
                            <div class="row text-center">
                                <figure class="figure">
                                    <img src={{ asset('/'.$grill->img.'.jpg') }} alt="grill" class="figure-img img-fluid rounded" style="width:50%">
                                    <div>
                                        <p>Modelo: {{ $grill->modelo }}</p>
                                        <p>Caracteristicas: {{ $grill->descripcion }}</p>
                                    </div>
                                </figure>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
    </div>
</div>
{!! $map['js'] !!}
@endsection
